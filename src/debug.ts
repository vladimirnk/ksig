import { callable } from "./deep-types";

export function trace(level: number | string = 0, noStack: boolean = false) {
    const stack = new Error().stack;
    const stackLines = stack.split(/\n/g);
    stackLines.shift(); // The message;
    stackLines.shift(); // The position in `trace()` (this function)
    // Now we got stack pointing to the place of `trace()` call
    if (level) {
        if (typeof level === "number") {
            while (level && stackLines.length) stackLines.shift();
        } else {
            while (stackLines.length && !stackLines[0].includes(level)) {
                stackLines.shift();
            }
            if (!stackLines.length) {
                throw new Error(`Can't trace ${level}`);
            }
            stackLines.shift();
        }
    }
    return stackLines.length ? noStack ? stackLines.shift() : stackLines.join("\n") : "";
}

export namespace trace {
    export const Caller = 1;
    export function point(level: number | string = 0) {
        if (typeof level === "number") {
            level = level + 1;
        }
        return trace(level, true);
    }
}

export function track<T extends callable>(original: T, options: {
    onCall?: (...args: any[]) => void | (any[]),
    onResult?: (result: any) => any
} = {}): T {
    const trackName = name || original.name || trace(trace.Caller, true);
    const { onCall, onResult } = options;
    return (function (...args: any[]) {
        if (onCall) {
            const onCallReturned = onCall(...args);
            if (onCallReturned) {
                args = onCallReturned;
            }
        }
        let result = original.apply(this, args);
        if (onResult) {
            onResult(result);
        }
        return result;
    }) as T;
}
