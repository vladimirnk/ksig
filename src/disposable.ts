const IsDisposedKey = Symbol();
const DependsOn = Symbol();
const IsDependedOnBy = Symbol();

/**
 * A base class to implement any disposable classes.
 * Disposable means that an instance may be "disposed off" - at this point
 * it frees all used resources and is not usable anymore.
 * 
 * Any disposable object may depend on any other disposable objects.
 * Whenever an object is disposed off all it's dependants are disposed of too.
 */
class Disposable {
    private [IsDisposedKey]: boolean = false;
    private [DependsOn]: Disposable[] = [];
    private [IsDependedOnBy]: Disposable[] = [];

    /**
     * Is true after the object has been disposed off.
     */
    protected get isDisposed() { return this[IsDisposedKey]; }

    /**
     * You should override this method in inheriting class to implement
     * any custom resource-freeing logic you need. Don't forget to
     * call super.onDispose() to support further inheritance.
     */
    protected onDispose() { }

    /**
     * Use this method to dispose off this object. Note that if you call it
     * two or more times - all calles except the first one will have no effect
     * at all. "onDispose()" method is guaranteed to be called only once for
     * each object.
     */
    public dispose() {
        if (!this[IsDisposedKey]) {

            this[IsDisposedKey] = true;

            this[DependsOn].forEach(
                dependency => dependency[IsDependedOnBy]
                    .splice(dependency[IsDependedOnBy].indexOf(this), 1)
            );
            this[DependsOn] = null;
            [...this[IsDependedOnBy]].forEach(dependent => dependent.dispose());

            this.onDispose();
        }
    }

    /**
     * Add a dependency. When dependency is disposed off - it will dispose this
     * object. If you dispose of this object first - it will remove itself from
     * dependents list in all dependencies, so you don't have to worry about
     * anything - you may dispose off any objects in any order - it just works.
     */
    public depend(dependency: Disposable) {
        if (!this[IsDisposedKey]) {
            if (dependency[IsDisposedKey]) {
                this.dispose();
            } else {
                if (!this[DependsOn].includes(dependency)) {
                    this[DependsOn].push(dependency);
                    dependency[IsDependedOnBy].push(this);
                }
            }
        } else {
            throw new Error(`Trying to call 'depend' on a disposed instance.`);
        }
    }
}

export default Disposable;
