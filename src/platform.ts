let _isBrowser: boolean = null;
export function isBrowser() {
    if (_isBrowser === null) {
        _isBrowser = (new Function(
            "try {return !!(this && this.window === this);}catch(e){ return false;}"
        ))();
    }
    return _isBrowser;
}

export function isServer() { return !isBrowser(); }

export const globalNS: { [key: string]: any } = isBrowser() ? window : global;
