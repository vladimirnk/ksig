/**
 * Apply defaults to given object.
 */
function defs(target: any, ...defSources: any[]) {
    return defSources.reduce((result, source) => _defs(result, source), target);
}

function _defs(target: any, defsp: any) {
    if (target === null || target === undefined) {
        return defsp === undefined ? target : defsp;
    }
    switch (typeof target) {
        case "object":
            if (target instanceof Array) {
                // Arrays are not modified
                return target;
            } else {
                if (target.contructor === Object) {
                    // For plain-objects defaults are applied field-by field
                    if (defsp && defsp.constructor === Object) {
                        Reflect.ownKeys(defsp).forEach(key => {
                            target[key] = defs(target[key], defsp[key]);
                        });
                    } else {
                        // except when default is not a plain object itself
                        return target;
                    }
                } else {
                    // Class instances are returned as-is
                    return target;
                }
            }
        case "symbol":
        case "string":
        case "boolean":
        case "number":
        case "function":
            // In any other case (not an object, null or undefined) target
            // is returned as-is
            return target;
        default:
            throw new Error(`Unexpected target is ${typeof target}`);
    }
}

namespace defs {
    /**
     * Create a function that will apply given defaults to any passed object.
     */
    export function compile(...defsList: any[]) {
        return function (target?: any) {
            return defs(target, ...defsList);
        };
    }
}

export default defs;
