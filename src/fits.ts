import clone from "clone";
import stringify from "./stringify";

interface PatternPath {
  name: string;
  path: string[];
}

namespace PatternPath {
  export function init(name: string = ""): PatternPath {
    return {
      name,
      path: []
    };
  }

  export function serialize(pp: PatternPath) {
    return (pp.name ? `@${pp.name}:` : ".") + pp.path.join(".");
  }
}

type FitPath = PatternPath[];

namespace FitPath {
  export function init(patternName: string = ""): FitPath {
    return [PatternPath.init(patternName)];
  }
  export function forkKey(fp: FitPath, key: string) {
    const res = clone(fp);
    const lastPattern = res[res.length - 1];
    lastPattern.path.push(key);
    return res;
  }
  export function forkPattern(fp: FitPath, pattern: string) {
    const res = clone(fp);
    res.push(PatternPath.init(pattern));
    return res;
  }
  export function serialize(fp: FitPath) {
    return fp.map(PatternPath.serialize).join("");
  }
}

interface FitError {
  path: FitPath;
  test: string;
  message: string;
}

namespace FitError {
  export function serialize(error: FitError) {
    return `${FitPath.serialize(error.path)} - failed "${error.test}": ${error.message}`;
  }
}

interface FitResult {
  isOk: boolean;
  errors: FitError[];
}

namespace FitResult {
  export function init(): FitResult {
    return {
      isOk: true,
      errors: []
    };
  }
  export function addError(result: FitResult, error: Error, path: FitPath, test: string) {
    result.errors.push({
      path,
      message: error.stack,
      test
    });
    result.isOk = false;
    return result;
  }
  export function serialize(result: FitResult) {
    if (result.isOk) { return ""; }
    else {
      return result.errors.map(FitError.serialize).join("\n");
    }
  }
  export function exception(res: FitResult) {
    return new Error(serialize(res));
  }
}


type FitInvocator<T = any> = (subject: T, path: FitPath, results: FitResult) => boolean;

interface FitPattern {
  (subject: any): FitResult;
  assert(subject: any): void;
  predicate(subject: any): boolean;
  invoke(subject: any, path: FitPath, result: FitResult): boolean;
  patternName: string;
}

function safeInvoke(invocator: FitInvocator, subject: any, path: FitPath, result: FitResult, name: string) {
  let passed: boolean = false;
  try {
    passed = invocator(subject, path, result);
  } catch (error) {
    passed = false;
    FitResult.addError(result, error, path, name);
  }
  return passed;
}

namespace FitPattern {
  export function wrap(invocator: FitInvocator, name: string = "") {
    const invoke = (subject: any, path: FitPath, result: FitResult) => {
      const innerPath = name ? FitPath.forkPattern(path, name) : path;
      return safeInvoke(invocator, subject, innerPath, result, name);
    };
    const pattern: FitPattern = ((subject: any) => {
      const result: FitResult = FitResult.init();
      const path: FitPath = FitPath.init(name);
      invoke(subject, path, result);
      return result;
    }) as any;
    pattern.invoke = invoke;
    pattern.predicate = (subject: any) => pattern(subject).isOk;
    pattern.patternName = name;
    return pattern;
  };
  export function create(name: string, reqs: any) {
    return wrap((subject: any, path: FitPath, result: FitResult) => {
      return fitAny(subject, reqs, path, result);
    }, name)
  }
  export function is(subject: any): subject is FitPattern {
    return (
      typeof subject === "function" &&
      typeof subject.invoke === "function" &&
      subject.invoke.length === 3 &&
      typeof subject.patternName === "string"
    );
  }
}

function isPlainObject(subject: any): subject is { [key: string]: any } {
  return (
    subject && typeof subject === "object" && subject.constructor === Object
  );
}

enum FitType {
  Equals,
  Map,
  Pattern
}

function fitType(reqs: any): FitType {
  if (isPlainObject(reqs)) {
    return FitType.Map;
  } else if (FitPattern.is(reqs)) {
    return FitType.Pattern;
  } else {
    return FitType.Equals;
  }
}

function fitEquals(subject: any, value: any, path: FitPath, result: FitResult) {
  if (subject !== value) {
    FitResult.addError(result, new Error(`${stringify(subject)} is not equal`), path, `equals ${stringify(value)} `);
    return false;
  } else {
    return true;
  }
}

function fitPattern(
  subject: any,
  pattern: FitPattern,
  path: FitPath,
  result: FitResult
) {
  if (FitPattern.is(pattern)) {
    return pattern.invoke(subject, path, result);
  } else {
    throw new Error(`Not a FitPattern: ${stringify(pattern)} `);
  }
}

function fitMap(
  subject: any,
  requirements: { [key: string]: any },
  path: FitPath,
  result: FitResult
) {
  let passed: boolean = true;
  if (isPlainObject(subject)) {
    Object.keys(requirements).forEach(requirementKey => {
      const requirement = requirements[requirementKey];
      const localSubject = subject[requirementKey];
      const localPath = FitPath.forkKey(path, requirementKey);
      passed = passed && fitAny(localSubject, requirement, localPath, result);
    });
  } else {
    throw new Error(`Not a plain object: ${stringify(subject)} `);
  }
  return passed;
}

function fitAny(
  subject: any,
  requirement: any,
  path: FitPath,
  result: FitResult
) {
  switch (fitType(requirement)) {
    case FitType.Equals:
      return fitEquals(subject, requirement, path, result);
    case FitType.Map:
      return fitMap(subject, requirement, path, result);
    case FitType.Pattern:
      return fitPattern(subject, requirement, path, result);
  }
}

function fitPeek(
  subject: any,
  requirement: any,
) {
  const path = FitPath.init();
  const res = FitResult.init();
  fitAny(subject, requirement, path, res);
  return res.isOk
}

function fits(subject: any, requirement: any): FitResult;
function fits(requirement: any): FitPattern;
function fits(...args: any[]) {
  let [subject, requirement] = args;
  if (args.length === 2) {
    if (FitPattern.is(requirement)) {
      return requirement(subject);
    } else {
      const result = FitResult.init();
      const path = FitPath.init();
      safeInvoke((subject: any, path: FitPath, result: FitResult) => {
        return fitAny(subject, requirement, path, result);
      }, subject, path, result, "");
      return result;
    }
  } else {
    if (FitPattern.is(subject)) {
      return subject;
    } else {
      return fits.pattern(subject);
    }
  }
}

const _isPlainObject = isPlainObject;

namespace fits {
  export const serialize = FitResult.serialize;
  export const error = FitResult.exception;
  export const isOk = (res: FitResult) => res.isOk;


  export type Pattern = FitPattern;
  export function pattern(reqs: any): Pattern;
  export function pattern(name: string, reqs: any): Pattern;
  export function pattern(...args: any[]) {
    let [name, reqs] = args;
    if (args.length === 1) {
      reqs = name;
      name = "";
    }
    if (typeof reqs === 'function') {
      name = name || reqs.name || "";
      return FitPattern.wrap(reqs, name);
    } else {
      return FitPattern.create(name, reqs);
    }
    
  }

  export type Result = FitResult;

  export type JSTypeName =
    | "undefined"
    | "boolean"
    | "number"
    | "string"
    | "symbol"
    | "function"
    | "object";

  export const conditional = (condition: FitInvocator, requirement: any) => {
    return fits.pattern("conditional", (subject: any, path: FitPath, result: FitResult) => {
      if (condition(subject, path, result)) {
        return fitAny(subject, requirement, path, result);
      } else {
        return true;
      }
    });
  }

  export function or(...reqs: any[]) {
    return fits.pattern("or", (subject: any, path: FitPath, results: FitResult) => {
      for (let i = 0; i < reqs.length; i++) {
        if (fitPeek(subject, reqs[i])) {
          return true;
        }
      }
      for (let i = 0; i < reqs.length; i++) {
        fitAny(subject, reqs[i], path, results);
      }
      return false;
    });
  }

  export function and(...reqs: any[]) {
    return fits.pattern("and", (subject: any, path: FitPath, results: FitResult) => {
      for (let i = 0; i < reqs.length; i++) {
        if (!fitPeek(subject, reqs[i])) {
          return fitAny(subject, reqs[i], path, results);
        }
      }
      return true;
    });
  }

  export const isObject = fits.pattern("isObject", x => x instanceof Object);
  export const isNull = fits.pattern("isNull", x => x === null);
  export const isUndefined = fits.pattern("isUndefined", x => x instanceof Object);
  export const isVoid = or(isNull, isUndefined);
  export const isArray = fits.pattern("isArray", x => x instanceof Array);
  export const isPlainObject = fits.pattern("isPlainObject", _isPlainObject);
}
export default fits;
