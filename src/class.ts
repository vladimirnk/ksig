export interface MultiArgClass<C = object> {
  new (...args: any[]): C;
  name: string;
}
export interface NoArgClass<C = object> {
  new (): C;
  name: string;
}
export type Class<C = object> = MultiArgClass<C> | NoArgClass<C>;
export type ClassType<C = object> = Class<C> | Function;
// export type InstanceType = object;

/**
 * Care: only checks if the argument is a function. Provided with a function
 * which is not used as a constructor will produce incorrect results
 */
export function isClass(object: any): object is ClassType {
  return typeof object === "function";
}

/**
 * Care: expects argument to be of an 'object type'. Thus if an class instance
 * is function - will produce incorrect results.
 */
export function isInstance(object: any): object is object {
  return object && typeof object === "object" && isClass(object.constructor);
}

/**
 * Returns a class of an instance, the class itself for a class
 * and null otherwise
 */
export function getClassOf(instanceOrClass: any): ClassType {
  if (isClass(instanceOrClass)) {
    return instanceOrClass;
  } else {
    if (isInstance(instanceOrClass)) {
      return instanceOrClass.constructor;
    } else {
      return null;
    }
  }
}

/**
 * returns a list of a classes from the last descandant to the first ancestor,
 * or given baseClass, excluding baseClass
 */
export function listClassesOf(instance: any, baseClass: ClassType = null) {
  let protoClass = getClassOf(instance);
  const result: ClassType[] = [];
  while (protoClass && protoClass !== baseClass) {
    result.push(protoClass);
    protoClass = getSuper(protoClass);
  }
  return result;
}

/**
 * Returns true if given class has second argument in it's inheritance chain
 */
export function isInheriting(theClass: any, ancestor: any) {
  return isClass(theClass) && isClass(ancestor)
    ? listClassesOf(theClass).includes(getClassOf(ancestor))
    : false;
}

/**
 * Remember: it gets the correct super because a class is passed in first place,
 * for instance or prototype it would be a prototype of super-class
 */
function getSuper(cls: ClassType): ClassType {
  return Object.getPrototypeOf(cls);
}

/**
 * Returns the first ancestor of the class of given argument
 */
export function getSuperClassOf(something: any): ClassType {
  const classOf = getClassOf(something);
  return classOf ? getSuper(classOf) : null;
}

/**
 * Copies all statics of the first ancestor to self
 */
export function inheritStatics(theClass: ClassType) {
  const superClass = getSuper(theClass);
  for (const key of Reflect.ownKeys(superClass)) {
    if (!(key in theClass)) theClass[key] = superClass[key];
  }
}

/** returns full(including ancestors) className */
export function fullClassName(cls: Function, delim = "/") {
  let name = "";
  while (cls && cls !== Object && cls !== Array && cls !== Function) {
    name = name ? `${name}${delim}${cls.name}` : cls.name;
    cls = Object.getPrototypeOf(cls.prototype).constructor;
  }
  return name;
}

export interface DecorateOptions<TClass extends { new (...args: any[]): any }> {
  modifyClass?: (cls: TClass) => void;
  modifyInstance?: (inst: InstanceType<TClass>, theClass: TClass) => any;
  inheritStatics?: boolean;
}

export function ClassDecorator<TClass extends { new (...args: any[]): any }>(
  options: DecorateOptions<TClass> = {}
) {
  const {
    modifyClass = () => undefined,
    modifyInstance = () => undefined,
    inheritStatics: doInheritStatics = true
  } = options;

  return ((theClass: TClass) => {
    class DecoratedClass extends theClass {
      constructor(...args: any[]) {
        super(...args);
        return modifyInstance(this as any, DecoratedClass) || this;
      }
    }
    const decoratedClassPrototype = Object.create(theClass.prototype);
    decoratedClassPrototype.constructor = DecoratedClass;

    DecoratedClass.prototype = decoratedClassPrototype;

    // theClass.prototype = decoratedClassPrototype; // needed?

    // Inherit statics
    if (doInheritStatics) {
      inheritStatics(DecoratedClass);
    }

    modifyClass(DecoratedClass);

    return DecoratedClass;
  }) as ClassDecorator;
}

export function PassiveClassDecorator<
  TClass extends { new (...args: any[]): any }
>(decorator: (cls: TClass) => void) {
  return ClassDecorator({
    modifyClass: decorator,
    inheritStatics: true
  });
}

export function ActiveClassDecorator<
  TClass extends { new (...args: any[]): any }
>(decorator: (inst: InstanceType<TClass>, theClass: TClass) => any) {
  return ClassDecorator({
    modifyInstance: decorator,
    inheritStatics: true
  });
}

export function ClassMethodDecorator<T>(
  decorator: (
    method: Function,
    name: string,
    container: any,
    descriptor: PropertyDescriptor
  ) => Function | PropertyDescriptor | void
): MethodDecorator {
  return function(
    container: any,
    propertyName: string,
    descriptor: PropertyDescriptor
  ): any {
    const originalMethod = container[propertyName];
    const decorated: Function | PropertyDescriptor | void = decorator(
      originalMethod,
      propertyName,
      container,
      descriptor
    );
    if (decorated) {
      if (typeof decorated === "function") {
        container[propertyName] = decorated;
      } else {
        return decorated;
      }
    }
  } as any;
}

export function ClassPropertyDecorator<T>(
  decorator: (
    container: T,
    key: string,
  ) => void
): PropertyDecorator {
  return function(
    container: T,
    propertyName: string
  ): any {
    decorator(container, propertyName);
  } as any;
}

export function PassiveParameterDecorator<T>(
  decorator: (value: T, index: number, args: any[]) => void
): ParameterDecorator {
  return function(target: any, method: string, index: number) {
    const originalMethod = target[method];
    target[method] = function(...args) {
      decorator(args[index], index, args);
      return originalMethod.apply(this, args);
    };
  };
}
export function ActiveParameterDecorator<T>(
  decorator: (value: T, index: number, args: any[]) => T
): ParameterDecorator {
  return function(target: any, method: string, index: number) {
    const originalMethod = target[method];
    target[method] = function(...args) {
      const newValue = decorator(args[index], index, args);
      args[index] = newValue;
      return originalMethod.apply(this, args);
    };
  };
}



