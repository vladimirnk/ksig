import objectPath from "object-path";

import bound from './bound';
import { DeepPartial } from "./deep-types";
import fits from './fits';
import Disposable from "./disposable";

const SignalUnsubscribe = Symbol();
const SignalOk = Symbol();
const SignalStop = Symbol();
const SignalDispose = Symbol();
const SignalAll = Symbol();

//  tslint:disable:unified-signatures
const QueueLimit = 4096;
const SyncInvokesLimit = 100;

function includesAll(arr: any[], ...vals: any[]) {
    return (arr instanceof Array) && (vals instanceof Array)
        && vals.reduce((ok, v) => ok && arr.includes(v), true);
}

const identityEquals = (a: any, b: any) => a === b;

// ToDo: async emission
// ToDo: async blocking handlers

/**
 * Represents a certain kind and source of signal. Signal acts like an event-emitter
 * for single event and like a primitive model - an observable model of a single field/value.
 */
export class Signal<TArgument = any> extends Disposable {

    constructor(options: Signal.Options<TArgument> = {}) {
        super();
        this._equals = options.equals || identityEquals;
        this._notUndefined = options.notUndefined || false;
        this._notNull = options.notNull || false;

        this.emit = bound.make((eventArg?: TArgument) => {
            if (this._notUndefined && eventArg === undefined)
                throw new Error(`Argument must be defined`);
            if (this._notNull && eventArg === null)
                throw new Error(`Argument must be not null`);
            if (!this._isSet) {
                this._isSet = true;
                this._firstValue = eventArg;
            }
            this._isSet = true;
            this._lastValue = eventArg;

            return this._container.invoke(eventArg);
        }, this) as Signal.Emitter<TArgument>;
        this._container = new Signal.ListenerContainer(this);

    }

    private _isSet: boolean = false;
    private _lastValue: TArgument = undefined;
    private _firstValue: TArgument = undefined;

    private _equals: Signal.Equals<TArgument>;
    private _container: Signal.ListenerContainer<TArgument>;
    private _notUndefined: boolean;
    private _notNull: boolean;

    /**
     * Emits event with given argument. This invokes all appropriate handlers.
     * Will not invoke any handlers if event is disabled (locked).
     * This method is bound: it will always have this signal as context.
     * And if it is used as listener for another signal - the listener will be
     * dropped automatically if this signal is disposed of.
     */
    emit: Signal.Emitter<TArgument>;

    /**
     * Returns last emitted value if any was emitted.
     * This method is bound: it will always have this signal as context.
     */
    @bound
    get(): TArgument {
        return this._lastValue;
    }

    /**
     * Similar to emit, but it will not call emit if the argument passed is 'equal'
     * to the last emitted value. You may define what 'equal' means by passing an
     * appropriate 'equals' method in Signal constructor options. By default '==='
     * is used to check equality.
     * This method is bound: it will always have this signal as context.
     */
    @bound
    set(value: TArgument) {
        if (!this._equals(value, this._lastValue)) {
            this.emit(value);
        }
        return this;
    }

    /**
     * Subscribe a signal handler which is invoked every time a signal is emitted.
     * If you pass a handler with two parameters, then a new signal called product signal
     * is created for this listener and it's `emit` method is passed to the handler as second
     * argument. The product signal is disposed with listener. This way you may implement
     * any chainable and composable signal tranformations and filtering. The product signal is
     * returned from 'on' method instead of created listener.
     * @param handler - function to invoke with signal
     * @param context - a context of the function. It is passed as this at call.
     *      Also if it's a `Disposable` instance then disposing of context leads.
     *      to disposing the created here listener.
     * @returns either signal listener or a product signal if you passed in a
     *      handler with two arguments. A signal listener is only used for signing off the
     *      handler.
     */
    on(
        handler: Signal.BasicHandler<TArgument>,
        context?: any
    ): Signal.Listener<TArgument>;
    on<TResult = TArgument>(
        handler: Signal.AdvancedHandler<TArgument, TResult>,
        context?: any
    ): Signal<TResult>;
    on<TResult = TArgument>(
        handler: Signal.Handler<TArgument, TResult>,
        context?: any
    ): Signal<TResult> | Signal.Listener<TArgument> {

        const listener = this._container.add(
            new Signal.Listener<TArgument, any>(handler, context)
        );

        return listener.product || listener;
    }

    /**
     * This signal emission immidiately triggers other signal's emission.
     * Both signalsare still independent - meaning if one of signals is disposed the
     * other wont be disposed automatically, only the listener will.
     * @param target - other signal
     * @returns the target signal
     */
    pipe(target: Signal<TArgument>) {
        this.on(target.emit);
        return target;
    }

    /**
     * This signal will be dependent on source signal, and mirrors anything it emits.
     * @param {Signal<TArgument>} from
     * @returns {Signal<TArgument>}
     */
    source(from: Signal<TArgument>) {
        from.on(this.emit);
        this.depend(from);
        return from;
    }

    /**
     * The values of two signals are synchronized.
     * These signals are interdependent, so if any of them is disposed of the other one is
     * disposed of too.
     * @param target
     * @returns target
     */
    link(target: Signal<TArgument>) {
        this.on(target.set);
        target.on(this.set);
        // this.depend(target);
        // target.depend(this);
        return target;
    }

    /**
     * Removes listener by either handler, context or listener.
     */
    off(target?: Signal.Handler<TArgument> | Signal | Signal.Listener): void {
        this._container.remove(target);
    }

    /**
     * Removes all listeners
     */
    reset() {
        this._container.remove(Signal.All);
    }

    /**
     * Freeform signal tranformation
     * @param transformator - a transformation to be applied to the signal
     * @returns a new signal of a transformed type
     */
    transform<TTarget = TArgument>(
        transformator: Signal.Transform<TArgument, TTarget>
    ): Signal<TTarget> {
        return this.on((arg: TArgument, emit: Signal.Emitter<TTarget>) => {
            emit(transformator((arg)));
        });
    }

    /**
     * Like `transform` but also sets initial value
     * @param transformator
     * @returns new signal
     */
    map<TTarget = TArgument>(
        transformator: Signal.Transform<TArgument, TTarget>
    ): Signal<TTarget> {
        return this.transform(transformator).set(transformator(this.get()));
    }

    /**
     * Ignore signals not matching predicate
     * @param {Signal.Matcher<TArgument>} predicate
     * @returns {Signal<TArgument>}
     */
    filter(predicate: Signal.Matcher<TArgument>) {
        return this.on((arg, emit: Signal.Emitter<TArgument>) => {
            if (predicate(arg)) {
                emit(arg);
            }
        });
    }

    /**
     * Filter signals based on "fits" duck-typing pattern. See the "fits" module for more
     * details.
     * @param {DeepPartial<TArgument>} pattern
     * @returns {Signal<TArgument>}
     */
    fits(pattern: DeepPartial<TArgument> | any) {
        return this.filter((arg: TArgument) => fits(arg, pattern).isOk);
    }

    /**
     * Merge to single signal source, emiting signal from both the original signal and the one
     * that is provided as the argument.
     * @param {Signal<T2>} second
     * @returns {Signal<Partial<Partial<TArgument> & Partial<T2>> & Partial<T2>>}
     */
    concat<T2>(second: Signal<T2>) {
        const signal = new Signal();
        signal.source(this);
        signal.source(second);
        return signal as Signal<DeepPartial<TArgument> & DeepPartial<T2>>;
    }
    limit(count: number): Signal<TArgument> {
        return this.on((arg, emit: Signal.Emitter<TArgument>) => {
            if (count) {
                count = count - 1;
                emit(arg);
                return Signal.Ok;
            } else {
                return Signal.Unsubscribe;
            }
        });
    }
    delay(time?: number) {
        let timeoutId: any;
        return this.on((arg, emit: Signal.Emitter<TArgument>) => {
            timeoutId = setTimeout(() => {
                emit(arg);
                timeoutId = undefined;
            }, time);

            const oldOnDispose = bound.getContext(emit).onDispose;
            bound.getContext(emit).onDispose = function (this: Signal) {
                if (timeoutId !== undefined) {
                    clearTimeout(timeoutId);
                    timeoutId = undefined;
                }
                return oldOnDispose.call(bound.getContext(emit));
            };
        });
    }
    debounce(time: number) {
        let lastEmit = 0;
        const bounce = (arg: TArgument, emit: Signal.Emitter<TArgument>) => {
            const emitTime = Date.now();
            if ((emitTime - lastEmit) > time) {
                lastEmit = emitTime;
                emit(arg);
            }
        };

        return this.on(bounce);
    }
    throttle(time: number) {
        let bounceEmit: TArgument;
        let bounceSet: boolean = false;
        let currentBounce: any = null;
        const bounce = (arg: TArgument, emit: Signal.Emitter<TArgument>) => {
            if (currentBounce === null) {
                emit(arg);
                bounceEmit = undefined;
                bounceSet = false;
                currentBounce = setTimeout(() => {
                    currentBounce = null;
                    if (bounceSet) {
                        bounce(bounceEmit, emit);
                    }
                }, time);
            } else {
                bounceSet = true;
                bounceEmit = arg;
            }
        };

        return this.on(bounce);
    }
    /**
     * Dervies an event which emits only once
     */
    get once(): Signal<TArgument> {
        const signal = this.on((arg, emit: Signal.Emitter<TArgument>) => {
            emit(arg);
            return Signal.Unsubscribe;
        });
        return signal;
    }
    get promise(): Promise<TArgument> {
        return new Promise<TArgument>((resolve, reject) => {
            try {
                this.on((arg, emit: Signal.Emitter<TArgument>) => {
                    try {
                        resolve(arg);
                        bound.getContext(emit).dispose();
                    } catch (error) {
                        reject(error);
                        bound.getContext(emit).dispose();
                    }
                });
            } catch (error) {
                reject(error);
            }
        });
    }

    occured(): Promise<TArgument> {
        if (this._isSet) {
            return Promise.resolve(this._firstValue);
        } else {
            return this.promise;
        }
    }

    asKey<T extends string>(key: T) {
        return this.transform(arg => ({ [key]: arg }));
    }
    field<TField extends keyof TArgument = keyof TArgument>
        (field: TField): Signal<TArgument[TField]> {
        return this.transform((arg: TArgument) => (arg && arg[field]) || undefined);
    }
    pick(...keys: string[]): Signal<DeepPartial<TArgument>> {
        return this.transform((arg: TArgument) => {
            const res: any = {};
            Object.keys(arg).forEach(key => {
                if (keys.includes(key)) {
                    res[key] = arg[key];
                }
            });
            return res;
        });
    }
    omit(...keys: string[]): Signal<DeepPartial<TArgument>> {
        return this.transform((arg: TArgument) => {
            const res: any = {};
            Object.keys(arg).forEach(key => {
                if (!keys.includes(key)) {
                    res[key] = arg[key];
                }
            });
            return res;
        });
    }

    nonVoid() {
        return this.filter((arg: TArgument) => arg !== undefined && arg !== null);
    }
    fieldContains(fieldName: string | string[], ...values: any[]) {
        return this.filter((arg: TArgument) =>
            includesAll(objectPath.get(arg, fieldName), ...values)) as any;
    }
    fieldEquals(fieldName: string, matchValue: any) {
        return this.filter((arg: TArgument) => {
            const fieldValue = objectPath.get(arg, fieldName);
            return fieldValue === matchValue;
        });
    }
    fieldMatches(fieldName: string, regex: RegExp) {
        return this.filter((arg: TArgument) => {
            const fieldValue = objectPath.get(arg, fieldName);
            return typeof fieldValue === 'string' && regex.test(fieldValue);
        });
    }
    fieldFits(fieldName: string | string[], pattern: any) {
        return this.filter((arg: TArgument) => {
            const fieldValue = objectPath.get(arg, fieldName);
            return fits(fieldValue, pattern).isOk;
        });
    }
    match(regex: RegExp) {
        return this.filter((arg: TArgument) => (typeof arg === 'string') && regex.test(arg));
    }

    extract(regex: RegExp, index: number = 1) {
        return this.match(regex).transform((x: any) => regex.exec(x)[index]);
    }

    private domElement: Element;
    private domEvent: string;
    private domHandler: any;

    static fromEventOnce(el: Element, event: string, passive: boolean = false) {
        const signal = new Signal<Event>();
        signal.domHandler = (e: Event) => {
            signal.emit(e);
            signal.dispose();
        };
        signal.domElement = el;
        signal.domEvent = event;
        el.addEventListener(
            signal.domEvent,
            signal.domHandler,
            { passive, capture: false, once: true }
        );
        return signal;
    }
    static fromEvent(el: Element, event: string, passive: boolean = false) {
        const signal = new Signal<Event>();
        signal.domHandler = (e: Event) => {
            signal.emit(e);
        };
        signal.domEvent = event;
        signal.domElement = el;
        el.addEventListener(
            signal.domEvent,
            signal.domHandler,
            { passive, capture: false }
        );
        return signal;
    }

    protected onDispose() {
        this.reset();

        this._equals = null;
        bound.forgetContext(this.emit);
        this._firstValue = undefined;
        this._lastValue = undefined;
        if (this.domElement) {
            this.domElement.removeEventListener(this.domEvent, this.domHandler);
            this.domEvent = null;
            this.domHandler = null;
            this.domElement = null;
        }
        super.onDispose();
    }
}

export namespace Signal {
    export interface Emitter<TArgument = any> extends bound.Method {
        (eventArg: TArgument): any;
    }
    export type BasicHandler<TArgument = any> = (eventArg: TArgument) => any;
    export type AdvancedHandler<TArgument = any, TResult = TArgument> =
        (eventArg: TArgument, emit: Emitter<TResult>) => any;
    export type Handler<TArgument = any, TResult = TArgument> =
        BasicHandler<TArgument> | AdvancedHandler<TArgument, TResult>;

    export function isAdvancedHandler(handler: Handler): handler is AdvancedHandler {
        return handler.length === 2;
    }

    export const Unsubscribe = SignalUnsubscribe;
    export const Stop = SignalStop;
    export const Dispose = SignalDispose;
    export const Ok = SignalOk;

    export const All = SignalAll;

    export class ListenerContainer<TArgument = any> {
        private listeners: Set<Listener<TArgument, any>> = new Set();
        constructor(private parent: Signal<TArgument>) {

        }
        add(listener: Listener<TArgument, any>) {
            this.listeners.add(listener);
            listener.container = this.listeners;
            listener.depend(this.parent);
            return listener;
        }
        remove(subj: any = All) {
            this.listeners.forEach(
                listener => listener.match(subj) && listener.dispose()
            );
        }

        private _isInvoking: boolean = false;
        private _invokeQueue: TArgument[] = [];
        private _syncInvokes: number = 0;
        invoke(arg?: any) {
            let invokeResult: any;
            if (this._isInvoking) {
                this._invokeQueue.push(arg);
                if (this._invokeQueue.length > QueueLimit) {
                    throw new Error(`Signal emission queue exceeded limit`);
                }
            } else {
                this._syncInvokes = this._syncInvokes + 1;
                this._isInvoking = true;
                for (const listener of this.listeners) {

                    invokeResult = listener.invoke(arg);

                    if (invokeResult === Dispose) {
                        this.parent.dispose();
                        break;
                    } else if (invokeResult === Stop) {
                        break;
                    }
                }
                this._isInvoking = false;
                if (this._syncInvokes > SyncInvokesLimit)
                    throw new Error(`Synchronous invocation limit exceeded`);
                if (this._invokeQueue.length) {
                    this.invoke(this._invokeQueue.shift());
                } else {
                    this._syncInvokes = 0;
                }
            }
            return invokeResult;
        }

    }

    export class Listener<
        TArgument = any,
        TResult = TArgument
        > extends Disposable {

        constructor(
            private _handler: Handler<TArgument, TResult>,
            private _context?: any
        ) {
            super();
            if (bound.is(this._handler)) {
                if (this._context) {
                    throw new Error(`Attempt to set context on a bound method.`);
                } else {
                    this._context = bound.getContext(this._handler);
                }
            }
            if (this._context instanceof Disposable) {
                this.depend(this._context);
            }
            if (isAdvancedHandler(this._handler)) {
                this._product = new Signal<TResult>();
                this._product.depend(this);
                this.depend(this.product);
            }
        }

        get product(): Signal<TResult> { return this._product; }

        match(subj: any = All) {
            return (subj === All) ? true : ((subj instanceof Listener)
                ? (subj === this)
                : (
                    (subj === this._context)
                    || (subj === this._handler)
                    || (subj === this._product)
                ));
        }
        invoke(arg?: TArgument) {
            const result = this._product
                ? this._handler.call(this._context, arg, this._product.emit)
                : this._handler.call(this._context, arg);
            if (result === Unsubscribe) {
                this.dispose();
            } else {
                return result;
            }
        }

        container: Set<Listener<TArgument, any>>;

        protected onDispose() {
            super.onDispose();
            if (this.container) {
                this.container.delete(this);
                this.container = null;
            }
            if (this._product) {
                this._product = null;
            }
            this._context = null;
            this._handler = null;
        }

        private _product: Signal<TResult>;
    }
    /**
     * The callback format used for adding _listeners to Event.
     */

    export type Transform<TData = any, TTarget = any> = (eventArg: TData) => TTarget;
    export type Matcher<TData = any> = (eventArg: TData) => boolean;

    export type Equals<TData = any> = (a: TData | any, b: TData | any) => boolean;

    export interface Options<TData> {
        equals?: Equals<TData>;
        notUndefined?: boolean;
        notNull?: boolean;
    }
}

export default Signal;
