import jss from "json-stringify-safe";
import clone from "clone";

function converter(key: string, value: any) {
    switch (typeof value) {
        case "object":
            return value || "<NULL>";
        case "function":
            return value.toString();
        case "undefined":
            return "<UNDEFINED>";
        case "number":
        case "boolean":
        case "string":
        case "symbol":
        default:
            return value;
        
    }
}

function stringify(subj: any) {
    return jss(subj, converter, 2);
}

namespace stringify {
    export function line(arg: any) {
        return jss(arg, converter);
    }
}

export default stringify;
